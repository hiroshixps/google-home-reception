const Botkit = require('botkit')
const googlehome = require('google-home-notifier')

//GoogleHome
const language = 'ja'
const deviceName = 'Google-Home' //なんでもいい
googlehome.device(deviceName, language)

//Slack
const slackToken = 'xoxb-333303616292-501031804962-VCmTfnze1uL4457HxGaM9Zmo' //ここにSlackToken

//Botkit
const controller = Botkit.slackbot({
    debug: false,
    status_optout: true
})
controller.spawn({
    token: slackToken
}).startRTM(err => {
    if (err) {
        throw new Error(err)
    }
})

controller.hears(['(.*)大野興業(.*)'], 'ambient,bot_message', (bot, message) => {
    console.log(message.text)
    googlehome.notify('大野にお客様です', res => {
        console.log(res)
    })
    bot.api.reactions.add({
        timestamp:message.ts,
        channel: message.channel,
        name:"ok"
    })
})
controller.hears(['(.*)メディカル(.*)'], 'ambient,bot_message', (bot, message) => {
    console.log(message.text)
    googlehome.notify('ジャパンメディカルにお客様です', res => {
        console.log(res)
    })
    bot.api.reactions.add({
        timestamp:message.ts,
        channel: message.channel,
        name:"ok"
    })
})
controller.hears(['(.*)配送(.*)'], 'ambient,bot_message', (bot, message) => {
    console.log(message.text)
    googlehome.notify('配送業者が来ています', res => {
        console.log(res)
    })
    bot.api.reactions.add({
        timestamp:message.ts,
        channel: message.channel,
        name:"ok"
    })
})
controller.hears(['sp(.*)'], 'ambient,bot_message', (bot, message) => {
    console.log(message.text)
    googlehome.notify(message.text.replace('sp',''), res => {
        console.log(res)
    })
    bot.api.reactions.add({
        timestamp:message.ts,
        channel: message.channel,
        name:"ok"
    })
})
